<?php
/**
 * Created by PhpStorm.
 * User: Ronald
 * Date: 2/23/15
 * Time: 9:05 AM
 */

namespace common\components;


use common\models\User;
use yii\web\ForbiddenHttpException;
use Yii;

class LoginHelper {

    public static function verifyGuid($guid)
    {
        if((null == $guid) || !User::validateGUID($guid))
            throw new ForbiddenHttpException('You cannot use this service. Please login via your unique link');
    }

    public static function validateGuid($model, $guid)
    {
        $user = User::findByUsername($model->username);
        if($guid == $user->guid){
            Yii::$app->session->set('user.role', $user->role);
            Yii::$app->session->set('user.guid', $guid);
            Yii::$app->session->setFlash('loggedIn', 'Success');
            return Yii::$app->controller->goBack();
        }else{
            Yii::$app->user->logout();
            throw new ForbiddenHttpException('You cannot use this service on this link');
        }
    }
}