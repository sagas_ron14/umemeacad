<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 5/26/15
 * Time: 9:39 AM
 */

namespace common\components;


use Yii;

class UmemeTools {

    public static function validateSession(){
        if(!Yii::$app->user->isGuest && !self::validateSessionVariables())
            Yii::$app->user->logout();
    }

    public static function validateSessionVariables(){
        return Yii::$app->session->has("user.role") && Yii::$app->session->has("user.guid");
    }

    public static function isManager(){

        return self::isLoggedIn() && (Yii::$app->session->get("user.role")=="manager");
    }

    public static function isLoggedIn()
    {
        if(Yii::$app->user->isGuest)
            Yii::$app->user->loginRequired();
        return !Yii::$app->user->isGuest;
    }

}