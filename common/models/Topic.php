<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "topic".
 *
 * @property integer $id
 * @property string $subject
 * @property string $created_on
 * @property integer $created_by
 * @property integer $category_id
 * @property integer $approved
 *
 * @property Category $category
 * @property User $createdBy
 * @property TopicPost[] $topicPosts
 */
class Topic extends \yii\db\ActiveRecord
{
    public static $active;
    private static $topics;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'topic';
    }

    public static function getPendingTopics()
    {
        $topics = self::getTopics();
        return $topics['pending'];
    }

    private static function populateTopics()
    {
        self::$topics = ['pending'=>[],'approved'=>[]];
        $command = "select count(p.id) as count, t.id as topic_id, t.subject as topic, p.approved as status from topic_post p, topic t where t.id = p.topic_id group by p.topic_id, p.approved";
        $result = Yii::$app->db->createCommand($command)->queryAll();
        if(count($result) > 0){
            $status = [0=>'pending',1=>'approved'];
            foreach($result as $row)
                self::$topics[$status[$row['status']]][] = [
                    'topic'=>$row['topic'],
                    'id'=>$row['topic_id'],
                    'count'=>$row['count']
                ];
        }
    }

    public static function getTopics(){
        if(!isset(self::$topics['pending']))
            self::populateTopics();
        return self::$topics;
    }

    public static function getApprovedTopics()
    {
        $topics = self::getTopics();
        return $topics['approved'];
    }

    public static function getTopicsInCategory($categoryId){
        $topicsArray = [];
        $topics = Topic::find()->where(['category_id'=>$categoryId])->all();
        if(count($topics) > 0)
            foreach ($topics as $topic)
                $topicsArray[] =  [
                    'topic'=>$topic->subject,
                    'id'=>$topic->id,
                    'count'=>count($topic->topicPosts),
                    'category_id'=>$categoryId
                ];
        return $topicsArray;
    }

    public static function getUserTopics($id)
    {
        if(!isset(self::$topics['user'][$id])){
            self::$topics['user'][$id] = [];
            $user = User::findIdentity($id);
            $categories = Category::getCategoriesWithUser($user);
            $categoryIds = implode(',', array_keys($categories));
            if($categoryIds){
                $command = "select count(p.id) as count, t.id as topic_id, t.subject as topic, p.approved as status from topic_post p, topic t where t.id = p.topic_id AND t.category_id in ({$categoryIds}) and p.approved = 1 group by p.topic_id, p.approved";
                $result = Yii::$app->db->createCommand($command)->queryAll();
                if(count($result) > 0){
                    foreach($result as $row)
                        self::$topics['user'][$id][] = [
                            'topic'=>$row['topic'],
                            'id'=>$row['topic_id'],
                            'count'=>$row['count']
                        ];
                }
            }
        }
        return self::$topics['user'][$id];
    }

    public static function getNewTopics()
    {
        $data = Topic::find()->asArray()->all();
        $topics =[];
        if(count($data) > 0){
            foreach($data as $topic)
                $topics[] = [
                    'topic'=>$topic['subject'],
                    'id'=>$topic['id'],
                ];
        }
        return $topics;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_on'], 'safe'],
            [['created_by', 'category_id'], 'integer'],
            [['subject'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Subject',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'category_id' => 'Category ID',
        ];
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopicPosts()
    {
        return $this->hasMany(TopicPost::className(), ['topic_id' => 'id']);
    }

    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {

            if($this->isNewRecord){
                $this->created_by = Yii::$app->user->id;
                $this->created_on = new Expression('CURRENT_TIMESTAMP');
            }
            return true;
        } else {
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Yii::$app->db->createCommand("DELETE FROM topic_post WHERE topic_id = '{$this->id}'")->execute();
            return true;
        } else {
            return false;
        }
    }
}
