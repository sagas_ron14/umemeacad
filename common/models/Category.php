<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $description
 *
 * @property Topic[] $topics
 * @property CategoryUser[] $categoryUsers
 * @property Category[] $categories
 * @property Category $parentCategory
 */
class Category extends \yii\db\ActiveRecord
{
    public static $active;
    private static $categories;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @return array
     */
    public static function getUserCategories($id)
    {
        $categories = [];
        $user = User::findOne($id);
        if($user != null){
            $cats = $user->categories;
            if(count($cats) > 0)
                foreach ($cats as $category)
                    $categories[] = [
                        'category'=>$category->name,
                        'id'=>$category->id,
                        'count'=>count($category->topics)
                    ];
        }
        return $categories;
    }

    public static function userCanSeeCategory($user_id, $category_id)
    {
        return (null != CategoryUser::find()->where("user_id='$user_id' and category_id='$category_id'")->one());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 400]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopics()
    {
        return $this->hasMany(Topic::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryUsers()
    {
        return $this->hasMany(CategoryUser::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategory()
    {
        return $this->hasOne(Category::className(), ['id'=>'parent_id']);
    }

    public function countTopics(){
        $result = Yii::$app->db->createCommand("select count(id) as count from topic where category_id='{$this->id}' group by category_id")->queryOne();
        if(!empty($result['count']))
            return $result['count'];
        return 'none';
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Yii::$app->db->createCommand("UPDATE topic SET category_id=NULL WHERE category_id = '{$this->id}'")->execute();
            Yii::$app->db->createCommand("DELETE FROM category_user where category_id='{$this->id}'")->execute();
            return true;
        } else {
            return false;
        }
    }

    public static function getCategoriesWithoutUser($user)
    {
        if(!isset(self::$categories[$user->id]['subscribable']))
            self::setCategories($user);
        return self::$categories[$user->id]['subscribable'];
    }

    /**
     * @param User $user
     */
    private static function setCategories($user)
    {
        if(!is_array(self::$categories))
            self::$categories = [];
        self::$categories[$user->id] = ['subscribed'=>[], 'subscribable'=>[]];

        $subscribed = $user->getCategories();
        $all = Category::find()->all();
        if(count($subscribed) > 0){
            self::$categories[$user->id]['subscribed'] = ArrayHelper::map($subscribed, 'id', 'name');
            $others = self::diff(self::$categories[$user->id]['subscribed'], ArrayHelper::map($all, 'id', 'name'));
            if(count($others) > 0)
                self::$categories[$user->id]['subscribable'] = $others;
        }
        else
            self::$categories[$user->id]['subscribable'] = ArrayHelper::map($all, 'id', 'name');
    }

    /**
     * @return array
    */
    public static function getCategoriesWithUser($user)
    {
        if(!isset(self::$categories[$user->id]['subscribed']))
            self::setCategories($user);
        return self::$categories[$user->id]['subscribed'];
    }

    /**
     * @param Group[] $subscribed
     * @param Group[] $all
     * @return Group[]
     */
    private static function diff($subscribed, $all)
    {
        $diff = [];
        foreach($all as $key => $value)
            if(!isset($subscribed[$key]))
                $diff[$key] = $value;

        return $diff;
    }

    public function getCategory(){
        return $this;
    }
}
