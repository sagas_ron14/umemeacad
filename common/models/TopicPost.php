<?php

namespace common\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "topic_post".
 *
 * @property integer $id
 * @property string $content
 * @property string $posted_on
 * @property integer $topic_id
 * @property integer $author_id
 * @property integer $approved
 *
 * @property User $author
 * @property Topic $topic
 */
class TopicPost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'topic_post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['posted_on'], 'safe'],
            [['topic_id', 'author_id', 'approved'], 'integer'],
            [['content'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'posted_on' => 'Posted On',
            'topic_id' => 'Topic ID',
            'author_id' => 'Author ID',
            'approved' => 'Approved',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(Topic::className(), ['id' => 'topic_id']);
    }

    public function saveNewPost($postParams, $approved=0)
    {
        $this->attributes = $postParams;
        $this->posted_on = new Expression("CURRENT_TIMESTAMP");
        $this->approved = $approved;
        return $this->save();
    }
}
