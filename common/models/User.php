<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $role
 * @property string $password write-only password
 * @property string $guid
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    private static $categories;

    private $_serverName;
    private $_passwordResetLink;
    private $_loginLink;
    private $_user_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public static function getRoleEnumList()
    {
        return [
            'user'=>'User',
            'manager'=>'Manager'
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return Category[]
     */
    public function getCategories()
    {
        if(!isset(self::$categories) || !is_array(self::$categories)){
            self::$categories = [];
            $result = Yii::$app->db->createCommand("select c.* from `category` c, category_user cu where cu.category_id = c.id and cu.user_id = '{$this->id}'")->queryAll();
            if(count($result) > 0)
                foreach($result as $row){
                    $category = new Category();
                    $category->attributes = $row;
                    $category->id = $row['id'];
                    self::$categories[] = $category;
                }
        }

        return self::$categories;
    }

    public static function generateGUID()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public static function validateGUID($uuid) {
        return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?' .
            '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
    }

    public function sendSignupEmail($password){
        $loginLink = $this->getLoginLink();
        $passwordReset = $this->getPasswordResetLink();
        $name = $this->getName();
        $serverName = $this->getServerName();
        $serverLink = Html::a($serverName, $this->getServerUrl());
        $message =
            "<html>
                <body>
                    <p>Welcome $name</p>
                    <p>You have been signed up to $serverLink.</p>
                    <p>
                        Your login link is <a href='$loginLink'>$loginLink</a>. You will only be able to login via this link and will be notified whenever the link is changed. Use the credentials below to login.<br>
                        Username: {$this->username} <br>
                        Password: {$password}
                    </p>
                    <p>You may change your password via <a href='$passwordReset'>$passwordReset</a> or another link provided by your administrator.</p>
                </body>
            </html>";
        $this->notifyUser('Welcome to '.$serverName, $message);
    }

    public function updateGuid()
    {

        $this->guid = self::generateGUID();
        $this->save();
        $loginLink = $this->getLoginLink();
        $username = $this->getName();
        $message =
            "<html>
                <body>
                    <p>Hi $username</p>
                    <p>Please note that you will login via <a href='$loginLink'>$loginLink</a> from now on.</p>
                </body>
            </html>";
        $this->notifyUser('Login Link Updated', $message);
    }

    private function notifyUser($subject, $content)
    {
        Yii::$app->mailer->compose()
            ->setFrom('hello@'.$this->getServerName())
            ->setTo($this->email)
            ->setSubject($subject)
            ->setHtmlBody($content)
            ->send();
    }

    public function sendLoginLink()
    {
        $this->guid = self::generateGUID();
        $this->save();
        $loginLink = $this->getLoginLink();
        $username = $this->getName();
        $message =
            "<html>
                <body>
                    <p>Hi $username</p>
                    <p>Your login link is <a href='$loginLink'>$loginLink</a></p>
                </body>
            </html>";
        $this->notifyUser('Login Link', $message);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Yii::$app->db->createCommand("DELETE FROM category_user WHERE user_id = '{$this->id}'")->execute();
            Yii::$app->db->createCommand("DELETE FROM topic_post WHERE author_id = '{$this->id}'")->execute();
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (($insert)) {

        }else{
            if(Yii::$app->user->id == $this->id){
                $identity = Yii::$app->user->getIdentity();
                Yii::$app->user->setIdentity($identity->findIdentity($this->id));
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    private function getLink($type){
        $link = '';
        switch($type){
            case 'login':
                $link =  str_replace('backend','frontend', Url::to(['/site/login', 'uid'=>$this->guid], true));
                break;
            case 'password-reset':
                $link = str_replace('backend','frontend', Url::to(['/site/request-password-reset', 'uid'=>$this->guid], true));

        }
        return str_replace('admin.', '', $link);
    }

    private function getServerName(){
        if(null == $this->_serverName)
            $this->_serverName = str_replace('admin.', '', $_SERVER['SERVER_NAME']);

        return $this->_serverName;
    }

    public function getLoginLink(){
        if(null == $this->_loginLink)
            $this->_loginLink =  $this->getLink('login');

        return $this->_loginLink;
    }

    private function getServerUrl(){
        return 'http://'.$this->getServerName();
    }

    private function getPasswordResetLink()
    {
        if(null == $this->_passwordResetLink)
            $this->_passwordResetLink = $this->getLink('password-reset');

        return $this->_passwordResetLink;
    }

    public function getName()
    {
        if($this->_user_name == null){
            $names = preg_split("/[\s,-_.,]+/", strtolower($this->username));
            $this->_user_name = ucwords(implode(' ', $names));
        }

        return $this->_user_name;
    }
}
