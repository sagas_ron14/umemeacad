<?php

namespace frontend\controllers;

use common\components\UmemeTools;
use common\models\Category;
use common\models\CategoryUser;
use common\models\Group;
use common\models\TopicPost;
use common\models\User;
use Yii;
use common\models\Topic;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TopicController implements the CRUD actions for Topic model.
 */
class TopicController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'approve' => ['post']
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function() {
                            return UmemeTools::isLoggedIn();
                        },
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all Topic models.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {

        $categoryId = Yii::$app->request->get('cat');
        $topicId = Yii::$app->request->get('top');
        $category = null;

        if($categoryId != null && !Category::userCanSeeCategory(Yii::$app->user->id, $categoryId))
            throw new ForbiddenHttpException('You shouldn\'t see topics this category');

        if(!is_numeric($categoryId)){
            $user = User::findOne(Yii::$app->user->id);
            $categories = $user->getCategories();
            if(count($categories) > 0)
                $category = $categories[0];
        }else
            $category = Category::findOne($categoryId);

        Category::$active = $category->id;

        if($category != null && (count($category->topics) > 0)){
            $topics = ArrayHelper::map($category->topics, 'id', 'subject');
            if(($topicId == null) && count($category->topics) > 0)
                return $this->actionView($category->topics[0]->id);
            elseif(isset($topics[$topicId]))
                return $this->actionView($topicId);
        }
        return $this->render('no_topics');

    }

    /**
     * Displays a single Topic model.
     * @param $topicId
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($topicId)
    {
        $topic = $this->findModel($topicId);
        Topic::$active = $topicId;

        if($topic == null)
            return $this->render('no_topics');
        else
            return $this->render('view', [
                'model' => $topic,
            ]);
    }

    /**
     * Finds the Topic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Topic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Topic::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPost(){
        $model = new TopicPost();
        if(null != Yii::$app->request->post('TopicPost'))
            $model->saveNewPost(Yii::$app->request->post('TopicPost'));

        return $this->redirect(Url::home());
    }
}
