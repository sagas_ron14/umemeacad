<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\CategoryUser;
use frontend\models\SignupForm;
use Yii;
use common\models\User;
use app\models\UserSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAccount(){

        $user = $this->findModel(Yii::$app->user->id);

        if (Yii::$app->request->post() != null){
            $user->username = $_POST['User']['username'];
            $user->email = $_POST['User']['email'];
            if($user->save())
                return $this->redirect(Url::home());
        }
        $this->layout = 'column1';

        return $this->render('@backend/views/user/account', ['model'=>$user]);
    }
}
