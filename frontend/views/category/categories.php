<?php
/**
 * Created by PhpStorm.
 * User: Ronald
 * Date: 2/16/15
 * Time: 4:53 PM
 */
use common\models\Category;
use yii\helpers\Html;?>

<?php $categories  = Category::getUserCategories(Yii::$app->user->id);?>

    <h4 class="topic-heading">Categories</h4>
    <?php if(count($categories) > 0){
            printCategories($categories);
          }else{?>
            <ul class="nav nav-sidebar">
                <li><?= Html::a("You are not in any category at the moment")?></li>
            </ul>
         <?php }?>

    <?php function printCategories($categories, $showBadge = false){?>
        <ul class="nav nav-sidebar">
            <?php foreach($categories as $category):?>
                <?php $badge = ($showBadge?"<span class='badge badge-success'>{$category['count'] }</span>":'')?>
                <li <?=((Category::$active == $category['id'])?'class="active"':'')?>>
                    <?= Html::a("{$category['category']} $badge", ['/topic/index', 'cat'=>$category['id']])?>
                </li>
            <?php endforeach?>
        </ul>
    <?php } ?>