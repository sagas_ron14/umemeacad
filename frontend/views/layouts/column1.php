<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$isLoggedIn = (!Yii::$app->user->isGuest);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => Html::img(Yii::$app->request->baseUrl.'/img/logo.png', ['style'=>'height:30px; display:inline']).' Umeme Academy Online',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [
                ['label' => 'Home', 'url' => ['/topic/index']],
            ];
            if($isLoggedIn && (Yii::$app->session->get('user.role') == 'manager'))
                $menuItems[] = ['label' => 'Admin', 'url' => '/admin'];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?php $navs = [
                ['Topics', 'topic'], ['Users', 'user'], ['Categories', 'category']
            ]?>
            <div class="site-index">
                <nav class="nav">
                    <ul class="nav nav-pills">
                        <?php
/*                        if($isLoggedIn)
                            foreach($navs as $nav):*/?><!--
                                <?php /*$style = ((Yii::$app->controller->id == $nav[1])?['class'=>'btn btn-default']:[])*/?>
                                <li><?/*= Html::a($nav[0], ['/'.$nav[1]], $style)*/?></li>
                            --><?php /*endforeach*/?>
                    </ul>
                </nav><hr>

                <div class="body-content">

                    <div class="row">
                        <div class="col-lg-2 sidebar">

                        </div>
                        <div class="col-lg-7">
                            <?= $content ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; Umeme <?= date('Y') ?></p>
        </div>
    </footer>
    <style>
        div.container div.navbar-header{
            background-color: #241e20;
        }
        .btn-success{
            background-color: #99b93b;
            border-color: #5cb85c;
        }
        a{
            color: #5cb85c;
        }
        a.hover, a.focus{
            color: #99b93b;
        }
        .navbar-inverse .navbar-nav > li > a:hover, a.navbar-brand:hover {
            color: #99b93b;
        }
        span.badge{
            background-color: #99b93b;
        }
        div.topic-view div.panel-group div.panel div.panel-heading{
            color: #58af58;
            background-color: #e2f4af;
            border-color: #5cb85c;
        }
    </style>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
