<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 5/25/15
 * Time: 4:56 PM
 */
use yii\helpers\Html; ?>
<div class="modal-dialog" style="width: 840px;">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Managing Director's Statement</h4>
        </div>
        <div class="modal-body">
            <div class="row" style="padding: 10px">
                <p>To: All Staff, <span class="pull-right">1<sup>st</sup> April, 2015 </span></p><br/>
                <p>Today is a very humbling day as the torch of leadership is passed on to me by
                    Charles  who  has  been  an  exceptional  and  motivational  leader.  I  want  to  thank
                    Charles for his courageous leadership and turnaround of this company.
                </p>
                <p>
                    I take this opportunity to thank our Board Chairman Patrick Bitature and the entire
                    Board for having the confidence to appoint me to lead Umeme into the next growth phase.
                </p>
                <p>
                    Join me in congratulating  Sam Zimbe  on his appointment  as Deputy  Managing
                    Director and Mrs Florence Nsubuga on her appointment as an Executive Director of
                    the company. Florence and Sam have been key pillars to the business in their
                    roles and look forward to their continued support to move the company forward.
                </p>
                <p>
                    As a team, we have a great opportunity to further build on successes achieved over
                    the last 10 years and support the government energy policies with respect to
                    increasing access from the current 15% to 40% by 2025 and improving efficiencies in
                    electricity pricing. The government  has committed significant  capital in building
                    hydro-power plants with over 1,000 Mwh to be brought on stream in the next 5
                    years. These will treble the available effective generation capacity. We have a critical
                    role to play on the distribution front.
                </p>
                <p>
                    Over the period, our safety record has improved, continued to attract, develop, and
                    retain  critical  talent.  We  continue  to  deliver  on  the  regulatory  targets,  have
                    committed significant investments in the network, and overall the customer
                    experience is improving. Our financials are strong, with a bankable balance sheet.
                    We cannot afford to rest on our laurels, but continue to tackle the current emerging
                    challenges and create more opportunities for sustained growth.
                </p>
                <p>
                    I begin my tenure as Managing Director with the confidence that we have a team of
                    talented and committed people to deliver the key organisational objectives. I truly
                    believe that each of us must find meaning in our work. The best work happens
                    when  you  know  that  it's  not  just  work,  but  something  that  will  improve  other
                    people's lives. This is the opportunity that drives each of us at this great company.
                    Over  the  next few  days  I will  be  visiting  all  the  teams  to be able  to share
                    the Company’s 2014 results, my vision and consult you on how to move this
                    company forward.
                </p>
                <p>Let me take this opportunity to wish you and your family happy Easter holidays</p>
                <p>Thank you</p>
            </div>
            <div class="row">
                <?=Html::img(Yii::$app->request->baseUrl.'/img/salestino-face.jpg', ['class'=>'col-xs-2'])?>
                <span class="col-xs-9">
                    <span class="col-md-12"><?=Html::img(Yii::$app->request->baseUrl.'/img/salestino-sign.png', ['class'=>'col-xs-5'])?></span>
                    <span class="col-md-12">Selestino Babungi</span>
                    <span class="col-md-12">Managing director</span>
                </span>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
