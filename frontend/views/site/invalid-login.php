<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 5/25/15
 * Time: 3:35 PM
 * @var $this yii\web\View
 */$this->title = 'Invalid Login Link'?>

    <div class="col-xs-10">
        <h4>Sorry,</h4>
        <h4>You seem to have the wrong login link.</h4>
        <h4>You need to login via your unique login link, which should have been provided to you via email.</h4>
        <h4>Please contact your administrator for more information if you think you're receiving this in error.</h4>
    </div>