<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 5/25/15
 * Time: 3:35 PM
 * @var $this yii\web\View
 */$this->title = 'No Login Link'?>

    <div class="col-xs-10">
        <h4>Hello,</h4>
        <h4>You need to login via your unique login link, which should have been provided to you via email.</h4>
        <h4>Please contact your administrator for more information.</h4>
    </div>