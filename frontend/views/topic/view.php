<?php

use common\models\TopicPost;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Topic */

$this->title = $model->subject;
$this->params['breadcrumbs'][] = ['label' => 'Topics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="topic-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <?php if(isset($model->topicPosts) && (count($model->topicPosts) > 0)){
            $formatter = new \yii\i18n\Formatter();
            $collapseId=-1;
            foreach($model->topicPosts as $post){
                if(($post->approved == 1) || ($post->author_id = Yii::$app->user->id)){
                    $collapseId = $post->id;
                    echo Yii::$app->controller->renderPartial('@app/views/topic/_post', ['post'=>$post, 'formatter'=>$formatter]);
                }
            }?>
            <?php if($collapseId == -1) $collapseId=$post->id;?>
            <script type="application/javascript">
                var lastPostId = 'post<?= $collapseId ?>';
                var lastPost = document.getElementById(lastPostId);
                lastPost.classList.add('in');
                window.location = '#' + lastPostId;
            </script>
        <?php } ?>
        <?= Yii::$app->controller->renderPartial('_newPost',[
            'model'=>new TopicPost(),
            'topic_id'=>$model->id,
        ])?>
    </div>
</div>
