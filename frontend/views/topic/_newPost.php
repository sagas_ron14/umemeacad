<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 2/18/15
 * Time: 3:01 PM
 * @var \common\models\TopicPost $model
 * @var integer $topic_id
 */
use vova07\imperavi\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$model->author_id = Yii::$app->user->id;
if(!isset($placeholder)) $placeholder = 'Write your opinion here . . .';
$model->topic_id = $topic_id;?>
<div class="post-form form">

    <?php $form = ActiveForm::begin(['action'=>Url::to(['/topic/post'])]); ?>

    <?=$form->field($model, 'content')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'en',
            'minHeight' => 130,
            'placeholder'=>$placeholder,
            'plugins' => [
                /*'clips',*/'fullscreen','video',
            ]
        ]
    ])->label('')?>

    <div class="form-group">
        <?= Html::submitButton('Post', ['class' => ($model->isNewRecord ? 'btn btn-success' : 'btn btn-primary') . ' pull-right']) ?>
    </div>

    <?= $form->field($model, 'author_id')->hiddenInput()->label('') ?>
    <?= $form->field($model, 'topic_id')->hiddenInput()->label('') ?>

    <?php ActiveForm::end(); ?>

</div>