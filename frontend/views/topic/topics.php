<?php
/**
 * Created by PhpStorm.
 * User: Ronald
 * Date: 2/16/15
 * Time: 4:53 PM
 */
use common\models\Category;
use common\models\Topic;
use yii\helpers\Html;?>

<?php $topics  = Topic::getTopicsInCategory(Category::$active);?>

    <h4 class="topic-heading">Topics</h4>
    <?php if(count($topics) > 0){
            printTopics($topics);
          }else{?>
            <ul class="nav nav-sidebar">
                <li><?= Html::a("There are no topics at the moment")?></li>
            </ul>
         <?php }?>

    <?php function printTopics($topics, $showBadge = false){?>
        <ul class="nav nav-sidebar">
            <?php foreach($topics as $topic):?>
                <?php $badge = ($showBadge?"<span class='badge badge-success'>{$topic['count'] }</span>":'')?>
                <li <?= ((Topic::$active == $topic['id']) ?'class="active"' :'') ?>>
                    <?= Html::a("{$topic['topic']} $badge", ['/topic/index', 'cat'=>$topic['category_id'], 'top'=>$topic['id']])?>
                </li>
            <?php endforeach?>
        </ul>
    <?php } ?>