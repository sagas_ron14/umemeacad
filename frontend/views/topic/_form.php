<?php

use common\models\Category;
use common\models\Group;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Topic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="topic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::merge([''=>'None'],ArrayHelper::map(Category::find()->all(),'id', 'name')))->label('Category') ?>

    <?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::merge([''=>'None'],ArrayHelper::map(Group::find()->all(),'id', 'name')))->label('Group') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
