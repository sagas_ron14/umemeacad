<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 2/17/15
 * Time: 2:29 PM
 * @var \common\models\TopicPost $post
 * @var \yii\i18n\Formatter $formatter
 */?>
<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="postHeading<?= $post->id ?>">
        <h4 class="panel-title">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#post<?= $post->id ?>" aria-expanded="true" aria-controls="post<?= $post->id ?>">
                <?= $post->author->username?>
                <span style="font-size: 12px" class="pull-right"><?= $formatter->asDatetime($post->posted_on, 'php:d F Y H:i:s A')?></span>
            </a>
        </h4>
    </div>
    <div id="post<?= $post->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="postHeading<?= $post->id ?>">
        <div class="panel-body">
            <?= $post->content?>
            <hr>
            <p>
                <i style="font-size: 12px">
                    posted on <?= $formatter->asDatetime($post->posted_on, 'php:d F Y H:i:s A')?>
                </i>
                <?php if($post->approved == 0)
                    \yii\helpers\Html::a('<span><i class="glyphicon glyphicon-check"></i> Approve</span>', ['approve', 'id'=>$post->id, 'back'=>$post->topic_id], ['class'=>'btn pull-right', 'data-method'=>'post', 'style'=>'color:green'])?>
            </p>
        </div>
    </div>
</div>