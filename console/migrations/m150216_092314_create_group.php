<?php

use yii\db\Schema;
use yii\db\Migration;

class m150216_092314_create_group extends Migration
{
    public function safeUp()
    {
        $this->createTable('group', [
            'id'=>'int not null primary key auto_increment',
            'name'=>'varchar(100)',
            'description'=>'varchar(255)',
        ]);
    }

    public function down()
    {
        echo "m150216_092314_add_group cannot be reverted.\n";

        return false;
    }
}
