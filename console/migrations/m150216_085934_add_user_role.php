<?php

use yii\db\Schema;
use yii\db\Migration;

class m150216_085934_add_user_role extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'role', "ENUM('user','manager') NULL DEFAULT 'user' AFTER updated_at");
    }

    public function down()
    {
        echo "m150216_085934_add_user_role cannot be reverted.\n";

        return false;
    }
}
