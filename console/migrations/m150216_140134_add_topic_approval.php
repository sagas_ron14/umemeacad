<?php

use yii\db\Schema;
use yii\db\Migration;

class m150216_140134_add_topic_approval extends Migration
{
    public function safeUp()
    {
        $this->addColumn('topic_post', 'approved', 'int(1) not null default 0');
    }

    public function down()
    {
        echo "m150216_140134_add_topic_approval cannot be reverted.\n";

        return false;
    }
}
