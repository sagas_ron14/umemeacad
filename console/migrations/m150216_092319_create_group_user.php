<?php

use yii\db\Schema;
use yii\db\Migration;

class m150216_092319_create_group_user extends Migration
{
    public function safeUp()
    {
        $this->createTable('group_user',[
            'id'=>'int not null primary key auto_increment',
            'group_id'=>'int',
            'user_id'=>'int',
        ]);
        $this->addForeignKey('fk_group_user_group_id','group_user','group_id','group','id');
        $this->addForeignKey('fk_group_user_user_id', 'group_user','user_id', 'user', 'id');
    }

    public function down()
    {
        echo "m150216_092319_add_group_user cannot be reverted.\n";

        return false;
    }
}
