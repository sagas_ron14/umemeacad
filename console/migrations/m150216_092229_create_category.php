<?php

use yii\db\Schema;
use yii\db\Migration;

class m150216_092229_create_category extends Migration
{
    public function safeUp()
    {
        $this->createTable('category',array(
            'id'=>'int not null primary key auto_increment',
            'name'=>'varchar(255)',
            'description'=>'varchar(400)',
        ));
    }

    public function down()
    {
        echo "m150216_092229_add_category cannot be reverted.\n";

        return false;
    }
}
