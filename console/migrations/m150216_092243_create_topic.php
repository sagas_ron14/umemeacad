<?php

use yii\db\Schema;
use yii\db\Migration;

class m150216_092243_create_topic extends Migration
{
    public function safeUp()
    {
        $this->createTable('topic', [
            'id'=>'int not null primary key auto_increment',
            'subject'=>'varchar(255)',
            'created_on'=>'timestamp not null',
            'created_by'=>'int',
            'category_id'=>'int',
            'group_id'=>'int',
        ]);
        $this->addForeignKey('fk_created_by','topic','created_by','user','id');
        $this->addForeignKey('fk_category_id','topic','category_id','category','id');
        $this->addForeignKey('fk_group_id','topic','group_id','group','id');

    }

    public function down()
    {
        echo "m150216_092243_add_topic cannot be reverted.\n";

        return false;
    }
}
