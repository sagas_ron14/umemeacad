<?php

use yii\db\Schema;
use yii\db\Migration;

class m150223_052443_add_user_guid extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'guid', 'varchar(100)');
    }

    public function down()
    {
        echo "m150223_052443_add_user_guid cannot be reverted.\n";

        return false;
    }
}
