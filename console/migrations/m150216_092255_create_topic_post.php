<?php

use yii\db\Schema;
use yii\db\Migration;

class m150216_092255_create_topic_post extends Migration
{
    public function safeUp()
    {
        $this->createTable('topic_post',[
            'id'=>'int not null primary key auto_increment',
            'content'=>'varchar(255)',
            'posted_on'=>'timestamp not null',
            'topic_id'=>'int',
            'author_id'=>'int',
        ]);
        $this->addForeignKey('fk_topic_id','topic_post','topic_id','topic','id');
        $this->addForeignKey('fk_author_id','topic_post','author_id','user','id');
    }

    public function down()
    {
        echo "m150216_092255_add_topic_post cannot be reverted.\n";

        return false;
    }
}
