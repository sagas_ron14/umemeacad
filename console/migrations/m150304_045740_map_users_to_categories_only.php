<?php

use yii\db\Schema;
use yii\db\Migration;

class m150304_045740_map_users_to_categories_only extends Migration
{
    public function safeUp()
    {

        $this->dropForeignKey('fk_category_id', 'topic');

        $this->dropTable('category');
        $this->renameTable('group', 'category');

        $this->dropForeignKey('fk_group_id', 'topic');
        $this->dropColumn('topic', 'category_id');
        $this->renameColumn('topic', 'group_id', 'category_id');
        $this->addForeignKey('fk_category_id', 'topic', 'category_id', 'category', 'id');

        $this->dropForeignKey('fk_group_user_group_id', 'group_user');
        $this->renameColumn('group_user', 'group_id', 'category_id');
        $this->renameTable('group_user', 'category_user');
        $this->addForeignKey('fk_category_user_category_id', 'category_user', 'category_id', 'category', 'id');

    }

    public function down()
    {
        echo "m150304_045740_map_users_to_categories_only cannot be reverted.\n";

        return false;
    }
}
