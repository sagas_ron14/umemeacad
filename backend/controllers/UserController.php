<?php

namespace backend\controllers;

use common\components\UmemeTools;
use common\models\Category;
use common\models\CategoryUser;
use frontend\models\SignupForm;
use Yii;
use common\models\User;
use app\models\UserSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function() {
                            return UmemeTools::isManager();
                        },
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $user = $this->findModel($id);
        $categoryUser = new CategoryUser();
        $categoryUser->user_id = $id;
        $subscribableCategories = Category::getCategoriesWithoutUser($user);
        $subscribedCategories = Category::getCategoriesWithUser($user);

        return $this->render('view', [
            'model' => $user,
            'categoryUser'=>$categoryUser,
            'subscribedCategories' => $subscribedCategories,
            'subscribableCategories' => $subscribableCategories,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }*/



    public function actionCreate()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()){
                $user->guid = User::generateGUID();
                $user->save();
                $post = Yii::$app->request->post('SignupForm');
                $user->sendSignupEmail($post['password']);
                return $this->redirect(['/user/view', 'id'=>$user->id]);
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $model->role = $post['User']['role'];
            if($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCategoriseUser(){
        $model = new CategoryUser();
        if(null != Yii::$app->request->post('CategoryUser')){
            $model->attributes = Yii::$app->request->post('CategoryUser');
            $model->save();
        }
        return $this->redirect(['/user/view', 'id'=>$model->user_id]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSetMissingGuids(){
        $users = User::find()->all();
        foreach($users as $user)
            if($user->guid == null)
                $user->updateGuid();
        echo 'OK';
    }

    public function actionResendLoginEmail($id){
        $user = $this->findModel($id);
        $user->sendLoginLink();
        return $this->redirect(['/user/view', 'id'=>$id]);
    }

    public function actionResetLoginLink($id){
        $user = $this->findModel($id);
        $user->updateGuid();
        return $this->redirect(['/user/view', 'id'=>$id]);
    }

    public function actionResetGuids(){
        $users = User::find()->all();
        foreach($users as $user)
            $user->updateGuid();
        return $this->redirect(['/user/index']);
    }

    public function actionRemoveFromCategory($category, $user)
    {
        CategoryUser::deleteAll([
            'category_id' => $category,
            'user_id' => $user,
        ]);
        return $this->redirect(['/user/view', 'id'=>$user]);
    }


    public function actionAccount(){

        $user = $this->findModel(Yii::$app->user->id);

        if (Yii::$app->request->post() != null){
            $user->username = $_POST['User']['username'];
            $user->email = $_POST['User']['email'];
            if($user->save())
                return $this->redirect(Url::home());
        }

        return $this->render('account', ['model'=>$user]);
    }

}
