<?php
namespace backend\controllers;

use common\components\LoginHelper;
use common\models\User;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->redirect(['/topic']);
        //return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest)
            return $this->goHome();

        $guid = Yii::$app->request->get('uid');
        try{
            LoginHelper::verifyGuid($guid);
        }catch (Exception $e){
            return $this->render('@frontend/views/site/no-login-link');
        }

        $model = new LoginForm();
        if($model->load(Yii::$app->request->post()) && $model->login())
            try{
                LoginHelper::validateGuid($model, $guid);
            }catch(Exception $e){
                return $this->render('@frontend/views/site/invalid-login');
            }
        else {
            return $this->render('login', [
                'model' => $model,
                'uid'=>$guid
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
