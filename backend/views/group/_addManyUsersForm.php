<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\GroupUser */
/* @var $users common\models\User[] */
/* @var $form yii\widgets\ActiveForm */
/* @var $group_id integer */
/* @var $label string */
/* @var $action string */
/* @var $button array*/
?>

<div class="group-form">

    <?php $form = ActiveForm::begin(['action'=>Url::to([$action, 'id'=>$group_id])]); ?>

    <?= $form->field($model, 'user_id')
        ->checkboxList(
            ArrayHelper::map($users, 'id', 'username'), ['separator'=>' '])
        ->hint('Check all users that apply')
        ->label($label, ['class'=>'group-user-title']) ?>

    <div class="form-group">
        <?= Html::submitButton($button['label'], ['class' => $button['class']]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
