<?php

use common\models\Group;
use common\models\GroupUser;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Group */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn-sm btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p><hr>
    <p><?= $model->description?></p>

    <?php if(isset($model->groupUsers) && (count($model->groupUsers) > 0))
            echo Yii::$app->controller->renderPartial('_addManyUsersForm', [
                'group_id'=>$model->id,
                'model'=>new GroupUser(),
                'users'=> ArrayHelper::map($model->groupUsers, 'user_id', 'user'),
                'label'=>'Users in this Group',
                'action'=>'remove-users',
                'button'=>['label'=>'Remove Users', 'class'=>'btn btn-danger']
            ]);?>

    <?php $usersNotInGroup = $model->getUsersNotInGroup();
        if(count($usersNotInGroup) > 0)
            echo Yii::$app->controller->renderPartial('_addManyUsersForm', [
                'group_id'=>$model->id,
                'model'=>new GroupUser(),
                'users'=>$usersNotInGroup,
                'label'=>'Users that can be added to this group',
                'action'=>'add-users',
                'button'=>['label'=>'Add Users', 'class'=>'btn btn-success']
            ]);?>

</div>
<style>
    .group-user-title{
        font-size: 16px;
        font-weight: 500;
        margin-top: 20px;
    }
</style>
