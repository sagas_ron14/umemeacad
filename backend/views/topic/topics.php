<?php
/**
 * Created by PhpStorm.
 * User: Ronald
 * Date: 2/16/15
 * Time: 4:53 PM
 */
use common\models\Topic;
use yii\helpers\Html;?>

<?php $pending  = Topic::getPendingTopics();
      $approved = Topic::getApprovedTopics();
       $count_pending_topics = count($pending);
       $count_approved_topics= count($approved)?>

<?php if($count_pending_topics > 0)
        printTopics('Posts Pending Approval', $pending)?>
<?php if($count_approved_topics > 0)
        printTopics((($count_approved_topics > 0)?'Approved':'All').' Posts', $approved, false)?>
<?php if(($count_pending_topics == $count_approved_topics) && ($count_pending_topics == 0) ){
            $newPosts = Topic::getNewTopics();
            if(count($newPosts) > 0)
                printTopics('Topics Without Posts', $newPosts, false);
            else{
                echo '<h4 class="topic-heading">No topics at the moment</h4>';
                echo '<ul class="nav nav-sidebar">';
                    echo '<li>'.Html::a('Create a topic', ['/topic/create'], ['class'=>'btn btn-default']).'</li>';
                echo '</ul>';
            }
        }?>

<?php function printTopics($title, $topics, $showBadge = true){?>
    <h4 class="topic-heading"><?= $title ?></h4>
    <ul class="nav nav-sidebar">
        <?php foreach($topics as $topic):?>
            <?php $badge = ($showBadge?"<span class='badge badge-success'>{$topic['count'] }</span>":'')?>
            <li><?= Html::a("{$topic['topic']} $badge", ['/topic/view', 'id'=>$topic['id']])?></li>
        <?php endforeach?>
    </ul>
<?php } ?>