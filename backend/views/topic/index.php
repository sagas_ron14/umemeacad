<?php

use common\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\i18n\Formatter;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Topics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel topic-panel">
    <div class="panel-body">
        <div class="topic-index">

            <h2><?= Html::encode($this->title) ?></h2>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Create Topic', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    [
                        'label'=>'Topic',
                        'attribute'=>'subject',
                        'format'=>'raw',
                        'value'=>function($data){
                            return Html::a($data->subject, ['/topic/view', 'id'=>$data->id]);
                        }
                    ],
                    [
                        'label'=>'Created On',
                        'attribute'=>'created_on',
                        'value'=>'created_on',
                    ],
                    [
                        'attribute'=>'category.name',
                        'label'=>'Category',
                        'filter'=>Html::activeDropDownList($searchModel, 'category_id', ArrayHelper::merge([''=>'Any'],ArrayHelper::map(Category::find()->all(),'id', 'name')),['class'=>'form-control']),

                    ],
                    [
                        'label'=>'Created By',
                        //'attribute'=>'created_by',
                        'format'=>'raw',
                        'value'=>function($data){
                            $user = \common\models\User::findOne($data->created_by);
                            return Html::a($user->username, ['/user/view', 'id'=>$data->id]);
                        }
                    ],
                    // 'group_id',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

        </div>
    </div>
</div>
