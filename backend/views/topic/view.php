<?php

use common\models\TopicPost;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Topic */

$this->title = ucwords($model->subject);
$this->params['breadcrumbs'][] = ['label' => 'Topics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="topic-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <?php if(isset($model->topicPosts) && (count($model->topicPosts) > 0)){
            $formatter = new \yii\i18n\Formatter();
            $collapseId=0;
            foreach($model->topicPosts as $post){
                if($collapseId == 0 && $post->approved == $collapseId)
                    $collapseId = $post->id;
                echo Yii::$app->controller->renderPartial('@app/views/topic/_post', ['post'=>$post, 'formatter'=>$formatter]);
            }?>
            <?php if($collapseId == 0) $collapseId=$post->id;?>
            <script type="application/javascript">
                var lastPostId = 'post<?= $collapseId ?>';
                var lastPost = document.getElementById(lastPostId);
                lastPost.classList.add('in');
                window.location = '#' + lastPostId;
            </script>
        <?php } else { ?>
            No user has posted upon this topic yet
        <?php } ?>
        <?= Yii::$app->controller->renderPartial('@frontend/views/topic/_newPost',[
            'model'=>new TopicPost(),
            'topic_id'=>$model->id,
            'placeholder'=>'Write your post here. This post will be auto approved',
        ])?>
    </div>
</div>
