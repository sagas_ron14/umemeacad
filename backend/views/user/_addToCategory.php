<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 2/17/15
 * Time: 5:42 PM
 *  @var \common\models\CategoryUser $categoryUser
 *  @var array $categories
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm; ?>

<div class="user-form ">

    <?php $form = ActiveForm::begin(['action'=>Url::to(['categorise-user'])]); ?>

    <div class="input-group form-inline">
        <?= $form->field($categoryUser, 'category_id')->dropDownList($categories, ['class'=>'form-control', 'style'=>'width:300px'])->label(false) ?>
        <div class="form-group input-group-btn">
            <?= Html::submitButton('Add to Category', ['class' => $categoryUser->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <input type="hidden" id="categoryuser-user_id" class="form-control" name="CategoryUser[user_id]" value="<?=$categoryUser->user_id?>">

    <?php ActiveForm::end(); ?>

</div>
<style>
    div.field-groupuser-group_id{
        max-height: 34px;
    }
</style>