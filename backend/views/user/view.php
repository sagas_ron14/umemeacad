<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $categoryUser common\models\CategoryUser */
/* @var $subscribableCategories array */
/* @var $subscribedCategories array */

$this->title = $model->getName();
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel user-panel">
    <div class="panel-body">
        <div class="user-view" xmlns="http://www.w3.org/1999/html">

            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-sm',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('Reset Login Link', ['reset-login-link', 'id' => $model->id], ['class' => 'btn btn-sm btn-default']) ?>
                <?= Html::a('Resend Login Link via Email', ['resend-login-email', 'id' => $model->id], ['class' => 'btn btn-sm btn-default']) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'username',
                    //'auth_key',
                    //'password_hash',
                    //'password_reset_token',
                    'email:email',
                    //'status',
                    //'created_at',
                    //'updated_at',
                    'role',
                    //['label'=>'Login Url', 'value'=>$model->getLoginLink()]
                ],
            ]) ?>

            <div class="panel user-category">

                <div class="panel-body">
                    <div>
                        <?php if(count($subscribableCategories) > 0):?>
                            <h4><?=$model->getName()?> can be added to the following categories</h4>
                            <?= Yii::$app->controller->renderPartial('_addToCategory', [
                                'categoryUser'=>$categoryUser,
                                'categories'=>$subscribableCategories
                            ])?><br>
                        <?php endif?>
                    </div>

                    <?php if(count($subscribedCategories) > 0):?>
                        <p><?=$model->getName()?> belongs to the following categories</p>
                        <ul class="list-group user-categories">
                            <?php foreach($subscribedCategories as $id => $category):?>
                                <li class="list-group-item">
                                    <span><?=Html::a('<i class="glyphicon glyphicon-trash"></i>', ['remove-from-category', 'category'=>$id, 'user'=>$model->id], ['class'=>'button', 'confirm'=>'Are you sure?'])?></span>
                                    <span><?=Html::a($category, ['category/view', 'id'=>$id])?></span>
                                </li>
                            <?php endforeach?>
                        </ul>
                    <?php endif?>
                </div>
            </div>

        </div>
    </div>
</div>
