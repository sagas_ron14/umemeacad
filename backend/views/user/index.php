<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel user-panel">
    <div class="panel-body">
        <div class="user-index">

            <h2><?= Html::encode($this->title) ?></h2>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a('Reset Login Links', ['reset-guids'], ['class' => 'btn btn-default']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    [
                        'label'=>'username',
                        'attribute'=>'username',
                        'format'=>'raw',
                        'value'=>function($data){
                            return Html::a($data->username,['view', 'id'=>$data->id]);
                        }
                    ],
                    //'auth_key',
                    //'password_hash',
                    //'password_reset_token',
                    'email:email',
                    'status',
                    // 'created_at',
                    // 'updated_at',
                    [
                        'label'=>'role',
                        'attribute'=>'role',
                        'filter'=>Html::activeDropDownList($searchModel, 'role', User::getRoleEnumList(),['class'=>'form-control'])
                    ],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

        </div>
    </div>
</div>
