<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
\common\components\UmemeTools::validateSession();
$isManager = (!Yii::$app->user->isGuest && (Yii::$app->session->get("user.role")=="manager"));
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => Html::img(Yii::$app->request->baseUrl.'/img/logo.png', ['style'=>'height:30px; display:inline']).' Umeme Academy Online (backend)',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top umeme-navigation-bar',
                ],
            ]);
            $menuItems = [
                ['label' => 'Home', 'url' => 'http://'.str_replace('admin.', '', $_SERVER['SERVER_NAME'])],
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => 'Hi ' . Yii::$app->user->identity->username ,
                    'linkOptions' => ['class'=>'main-nav-link'],
                    'items' => [
                        ['label'=>'Account', 'url' => ['/user/account']],
                        ['label'=>'Logout', 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post', 'class'=>'main-nav-link']],
                    ]
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?php Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?php $navs = [
                ['Topics', 'topic'], ['Users', 'user'], ['Categories', 'category'],
            ]?>
            <div class="site-index">
                <nav class="nav">
                    <ul class="nav nav-pills">
                        <?php
                        if($isManager)
                            foreach($navs as $nav):?>
                                <?php $style = ((Yii::$app->controller->id == $nav[1])?['class'=>'btn btn-default']:[])?>
                                <li><?= Html::a($nav[0], ['/'.$nav[1]], $style)?></li>
                            <?php endforeach?>
                    </ul>
                </nav><hr>

                <div class="body-content">

                    <div class="row">
                        <div class="col-lg-3 sidebar" style="border-right: 1px solid #c9c9c9">
                            <?php if($isManager):?>
                                <?= Yii::$app->controller->renderPartial('@app/views/topic/topics',[])?>
                            <?php endif?>
                        </div>
                        <div class="col-lg-9">
                            <?= $content ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p>
                <span class="pull-right"><?=Html::a('Managing Director\'s Statement', '#', [
                    'data-toggle'=>'modal',
                    'data-target'=>'#chairman-statement',
                ])?></span>
                <span class="pull-left">&copy; Umeme Academy <?= date('Y') ?></span>
            </p>
        </div>
    </footer>

    <!-- Modal -->
    <div class="modal fade " id="chairman-statement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <?=Yii::$app->controller->renderPartial('@frontend/views/site/md-statement')?>
    </div>

    <?php $this->endBody() ?>

    <?php if(Yii::$app->session->hasFlash('loggedIn')):?>
        <?php Yii::$app->session->removeFlash('loggedIn')?>
        <script>
            $(document).ready(function () {
                $('#chairman-statement').modal('show');
            });
        </script>
    <?php endif?>
</body>
</html>
<?php $this->endPage() ?>
