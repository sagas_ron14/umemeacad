<?php

use common\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel category-panel">
    <div class="panel-body">
        <div class="category-index">

            <h2><?= Html::encode($this->title) ?></h2>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    [
                        'label'=>'name',
                        'format'=>'raw',
                        'attribute'=>'name',
                        'value'=>function($data){
                            return Html::a($data->name, ['view', 'id'=>$data->id]);
                        }
                    ],
                    //'description',
                    [
                        'label'=>'Under Category', 'format'=>'raw',
                        'filter'=>ArrayHelper::map(Category::find()->where(['parent_id'=>null])->all(), 'id', 'name'),
                        'value'=>function($data){
                            return (isset($data->parent_id)?Html::a($data->parentCategory->name, ['view', 'id'=>$data->parentCategory->id]):'');
                        }
                    ],
                    [
                        'label' => 'Topics',
                        'value' => function($data){
                            return $data->countTopics();
                        }
                    ],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

        </div>
    </div>
</div>
