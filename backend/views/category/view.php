<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$no_topics = (isset($model->topics))?count($model->topics):0;
$no_categories = (isset($model->categories))?count($model->categories):0;
?>
<div class="panel category-panel">
    <div class="panel-body">
        <div class="category-view">

            <h3><?= Html::encode($this->title) ?> Category <?=(isset($model->parentCategory)?'(under '.Html::a($model->parentCategory->name, ['view', 'id'=>$model->parentCategory->id]).')':'')?></h3>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-sm',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this category?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <hr>

            <div class="panel category-panel">
                <div class="panel-heading title"><?= $no_topics?> topic<?= ($no_topics != 1)?'s':''?> under <?=$model->name?></div>
                <div class="panel-body">
                    <?php if($no_topics > 0):?>
                        <ul class="list-group category-topics">
                            <?php foreach($model->topics as $topic):?>
                                <li class="list-group-item">
                                    <span><?=Html::a('<i class="glyphicon glyphicon-trash"></i>', ['remove-topic', 'category'=>$model->id, 'topic'=>$topic->id], ['class'=>'button', 'confirm'=>'Are you sure?'])?></span>
                                    <span><?= Html::a($topic->subject, ['/topic/view', 'id'=>$topic->id])?></span>
                                </li>
                            <?php endforeach?>
                        </ul>
                    <?php endif?>
                </div>
            </div>

            <?php if(!isset($model->parent_id)):?>
            <hr>
            <div class="panel category-panel">
                <div class="panel-heading title"><?= $no_categories?> categor<?= ($no_categories != 1)?'ies':'y'?> under <?=$model->name?> <?=Html::a('Add sub-category', ['create', 'under'=>$model->id], ['class'=>'btn btn-success small pull-right'])?></div>
                <div class="panel-body">
                    <?php if($no_categories > 0):?>
                        <ul class="list-group category-topics">
                            <?php foreach($model->categories as $category):?>
                                <li class="list-group-item">
                                    <span><?=Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id'=>$category->id], ['class'=>'button',
                                            'data' => [
                                                'confirm' => 'Are you sure you want to delete this category?',
                                                'method' => 'post',
                                            ],])?>
                                    </span>
                                    <span><?= Html::a($category->name, ['/category/view', 'id'=>$category->id])?></span>
                                </li>
                            <?php endforeach?>
                        </ul>
                    <?php endif?>
                </div>
            </div>
            <?php endif?>

        </div>
    </div>
</div>
